import { Injectable } from '@angular/core';
import { cars } from './cars-mock';

@Injectable({
  providedIn: 'root',
})
export class CarsService {

  constructor() { }

  get() {
    return cars
  }
}
