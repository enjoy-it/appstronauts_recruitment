# Appstronauts

## Setup

- Install Node.js (at least version `10.8.0`)
- Install Angular CLI (version `6.2.4`)
- Run `npm install` to install dependencies

## Starting application

- Run `ng serve`, application will be available under `http://localhost:4200/`

## Zadanie

Po odpaleniu aplikacji zobaczysz listę 15 samochodów wraz z ich oceną. Niektóre samochody wyświetlane są więcej niż jeden raz.
W `cars.service.ts` zmodyfikuj funkcję `get()` w ten sposób, aby zwracała listę która:
 1. zawiera niepowtarzające się samochody, 
 2. zawiera samochody z oceną (rating) wyższą niż 50, 
 3. jest posortowana malejąco. 
 
Następnie każda z ocen samochodu powinna być zaokrąglona do liczby całkowitej.

Przykład:

Dane wejściowe:

```js
[
  {
    image: '../assets/bmw.jpg',
    name: 'BMW',
    type: 'Coupe',
    rating: 48.78,
    uuid: 'caefc890-c034-11e1-939f-005056a8759d',
  },
  {
    image: '../assets/aventador.jpg',
    name: 'Lamborghini Aventador',
    type: 'Exotic',
    rating: 60.07,
    uuid: 'bc9a55e1-a904-11e1-9412-005056900141',
  },
  {
    image: '../assets/charger.jpg',
    name: 'Dodge Charger',
    type: 'Muscle',
    rating: 58.27,
    uuid: 'ba27cf47-a904-11e1-9412-005056900141',
  },
  {
    image: '../assets/bmw.jpg',
    name: 'BMW',
    type: 'Coupe',
    rating: 48.78,
    uuid: 'caefc890-c034-11e1-939f-005056a8759d',
  },
  {
    image: '../assets/charger.jpg',
    name: 'Dodge Charger',
    type: 'Muscle',
    rating: 58.27,
    uuid: 'ba27cf47-a904-11e1-9412-005056900141',
  }
]
```

Wynik:
```js
[
  {
    image: '../assets/aventador.jpg',
    name: 'Lamborghini Aventador',
    type: 'Exotic',
    rating: 60,
    uuid: 'bc9a55e1-a904-11e1-9412-005056900141',
  },
  {
    image: '../assets/charger.jpg',
    name: 'Dodge Charger',
    type: 'Muscle',
    rating: 58,
    uuid: 'ba27cf47-a904-11e1-9412-005056900141',
  }
]
```

Samochody powinny wyświetlać się w postaci gridu uwzględniając przerwę między kolumnami `30px` i przerwę między wierszami `30px`. Grid powinien być responsywny i powinien zawierać maksymalnie 3 kolumny (zobacz `grid.png` w głownym katalogu projektu).
