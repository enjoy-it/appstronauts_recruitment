export interface Cars {
  image: string;
  name: string;
  type: string;
  rating: number;
  uuid: string;
}
