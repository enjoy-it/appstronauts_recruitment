import { Component, OnInit } from '@angular/core';
import { Cars } from './cars';
import { CarsService } from './cars.service';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.scss']
})
export class CarsComponent implements OnInit {
  public cars: Cars[] = [];

  constructor(private carssService: CarsService) { }

  ngOnInit() {
    this.cars = this.carssService.get();
  }

}
